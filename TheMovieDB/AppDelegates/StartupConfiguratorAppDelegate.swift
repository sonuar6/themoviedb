//
//  StartupConfiguratorAppDelegate.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import UIKit
import Alamofire

class StartupConfiguratorAppDelegate: AppDelegateType {
    let visualEffectView = UIVisualEffectView(effect: nil)
    let manager = Alamofire.NetworkReachabilityManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        setupNetworkReachability()
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
     
    }
    
    func setupNetworkReachability() {
        manager?.listener = { [weak self] status in
            guard let self = self else { return }
            switch status {
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
            case .notReachable:
                self.navigateToNetworkError()
                
            case .unknown :
                self.navigateToNetworkError()
                
            }
        }
        manager?.startListening()
    }
    
    func navigateToNetworkError() {
        if let topMost = UIViewController.topMost {
            let alert = UIAlertController(title: "Warning", message: "There is problem your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            topMost.present(alert, animated: true, completion: nil)
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIView.animate(withDuration: 0.5) {
            self.visualEffectView.removeFromSuperview()
            self.visualEffectView.effect = nil
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        if !self.visualEffectView.isDescendant(of: appDelegate.window!) {
             appDelegate.window?.addSubview(self.visualEffectView)
        }
        self.visualEffectView.frame = (appDelegate.window?.bounds)!

        UIView.animate(withDuration: 0.5) {
            self.visualEffectView.effect = UIBlurEffect(style: .light)
        }
    }
    
}
