//
//  ThirdPartiesConfiguratorAppDelegate.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import IQKeyboardManagerSwift

class ThirdPartiesConfiguratorAppDelegate: AppDelegateType {
    func application(
            _ app: UIApplication,
            open url: URL,
            options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {

           return true
        }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        configureKeyboardManager()
        configureConfigurationManager()

        return true
    }
    
    
    private func configureKeyboardManager() {
        IQKeyboardManager.shared.enable = true
    }
    
    private func configureConfigurationManager() {
        let app_configuration_v1 = "app_configuration_v1"
       
        let files = ConfigLocalFiles(bundleId: Bundle.main.bundleIdentifier ?? "", appConfig: app_configuration_v1)
        MDBConfigurationManager.configure(baseURL: Constants.URL.baseURL, configFiles: files)
        
        // Fetch AppConfiguration
        
    }
    
    
    
}
