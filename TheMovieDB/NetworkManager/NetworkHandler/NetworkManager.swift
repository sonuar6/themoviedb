//
//  NetworkManager.swift
//  EmpayCheckout
//
//  Created by emcuser on 10/07/2021.
//  Copyright © 2021 emcuser. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


enum NetworkEnvironment
{
    case dev,production,stage
}




class NetworkManager {
    
    
    
    static var networkEnviroment: NetworkEnvironment  {
        var env: NetworkEnvironment = .production
        #if TheMovieDBDev
        env = .dev
        #else
        env = .production
        #endif
        return env
    }
    private let sessionManager: SessionManager
    
    private static var shared: NetworkManager = {
        let apiManager = NetworkManager(sessionManager: SessionManager())
        return apiManager
    }()
    
    class func sharedManager() -> NetworkManager {
        return shared
    }
    
    private init(sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    
    
    
    
    
    
    //MARK:- Private methods
    
 
    
    func requestWithArrayBodyRestData<Model: Mappable, Error: Mappable>(endpoint: Endpoint,success:@escaping (Int)-> Void,failureHandler: @escaping (Error) -> Void, completionHandler: @escaping (Model) -> Void, errorHandler: @escaping (String) -> Void)-> DataRequest? {
        if Reachability.isConnectedToNetwork(){
            guard let urlRequest = buildRequest(with: endpoint) else {
                let fullURL = endpoint.baseURL + endpoint.path
                let error = AFError.invalidURL(url: fullURL)
                errorHandler(error.localizedDescription)
                return nil
            }
            
            let request = sessionManager.request(urlRequest)
            request.session.configuration.timeoutIntervalForRequest = 300
            request.session.configuration.timeoutIntervalForResource = 300
            #if DEBUG
            debugPrint("==========> START")
            debugPrint(request)
            debugPrint("==========> END")
            #endif
            request.validate(statusCode: 200..<300)
                .responseString { (response: DataResponse<String>) in
                    #if DEBUG
                    debugPrint("<========== START")
                    debugPrint(response)
                    debugPrint("<========== END")
                    #endif
            }.responseJSON { response  in
                
                if let resp = response.response{
                    success(resp.statusCode)
                }
               
                
                switch (response.result) {
                case .success:
                    self.processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut{
                        //errorHandler(error.localizedDescription)
                    }
                    self.processErrorResponse(response: response, failureHandler: failureHandler)
                    break
                    
                }
            }
            return request
            
        }else{
            errorHandler("Unable to connect to server. Please try again later.")
            return nil
        }
        
        
        
    }
    
    
    
    
    func requestRestData<Model: Mappable,Error: Mappable>(endpoint: Endpoint,success:@escaping (Int)-> Void, completionHandler: @escaping (Model) -> Void,failureHandler: @escaping (Error) -> Void, errorHandler: @escaping (String) -> Void)-> DataRequest? {
        if Reachability.isConnectedToNetwork(){
            
            guard let url = buildURL(with: endpoint) else {
                let fullURL = endpoint.baseURL + endpoint.path
                let error = AFError.invalidURL(url: fullURL)
                errorHandler(error.localizedDescription)
                return nil
            }
            let request = sessionManager.request(url, method: endpoint.method, parameters: endpoint.body, encoding: endpoint.encoding, headers: endpoint.headers)
            request.session.configuration.timeoutIntervalForRequest = 120
            request.session.configuration.timeoutIntervalForResource = 120
            
            #if DEBUG
            debugPrint("==========> START")
            debugPrint(request)
            debugPrint("==========> END")
            #endif
            request.validate(statusCode: 200..<300)
                .responseString { (response: DataResponse<String>) in
                    #if DEBUG
                    debugPrint("<========== START")
                    debugPrint(response)
                    debugPrint("<========== END")
                    #endif
            }.responseJSON { response  in
                if let resp = response.response {
                    success(resp.statusCode)
                }
               
                switch (response.result) {
                case .success:
                    self.processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(let error):
                    switch error._code {
                    case NSURLErrorTimedOut,NSURLErrorUnknown,NSURLErrorUnsupportedURL:
                        errorHandler("Request timed out")
                        break
                    default:
                        break
                    }
                    self.processErrorResponse(response: response, failureHandler: failureHandler)
                    break
                }
            }
            return request
        }else
        {
            errorHandler("Unable to connect to server. Please try again later.")
            return nil
        }
        
        
    }
    
    
    
    
    
    
    private  func processResponse<Model: Mappable>(response: DataResponse<Any>, completionHandler: @escaping (Model) -> Void, errorHandler: @escaping (String) -> Void) {
        
        
        switch response.result {
        case .success:
            if let JSON = response.result.value {
                
                if let requestResult = Mapper<Model>().map(JSONObject: JSON) {
                    completionHandler(requestResult)
                }
                else {
                   // errorHandler("There is something wrong parsing the object from server.")
                }
            }
        case .failure(let error):
            var errorString = error.localizedDescription
            if let data = response.data, let responseDataStr = String(data: data, encoding: String.Encoding.utf8) {
                errorString = responseDataStr
            }
            errorHandler(errorString)
        }
    }
    
    
    
    
    private  func processErrorResponse<Error: Mappable>(response: DataResponse<Any>, failureHandler: @escaping (Error) -> Void) {
        if let data = response.data, let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            if let requestResult = Mapper<Error>().map(JSONObject: json) {
                failureHandler(requestResult)
            }
        }
    }
    
    
}





extension NetworkManager {
    private func buildURL(with endpoint: Endpoint) -> URL? {
        guard let baseURL = URL(string: endpoint.baseURL) else { return nil }
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else { return nil }
        print(components)
        
        components.path = components.path + endpoint.version + endpoint.path
        // Create query parameters if HTTPMethod is GET OR DELETE
        var queryItems: [URLQueryItem] = []
        if endpoint.queryParams.count > 0 {
            for param in endpoint.queryParams {
                let newItems = param.map({ URLQueryItem(name: $0.key, value: String(describing: $0.value)) })
                queryItems.append(contentsOf: newItems)
            }
            components.queryItems = queryItems
        }
        
        guard let url = components.url else { return nil }
        return url
    }
    
    private func buildRequest(with endpoint: Endpoint) -> URLRequest? {
        guard let baseURL = URL(string: endpoint.baseURL) else { return nil }
        guard var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: true) else { return nil }
        components.path = components.path + endpoint.path
        // Create query parameters if HTTPMethod is GET OR DELETE
        var queryItems: [URLQueryItem] = []
        if endpoint.queryParams.count > 0 {
            for param in endpoint.queryParams {
                let newItems = param.map({ URLQueryItem(name: $0.key, value: String(describing: $0.value)) })
                queryItems.append(contentsOf: newItems)
            }
            components.queryItems = queryItems
        }
        
        guard let url = components.url else { return nil }
        
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue
        request.timeoutInterval = 300
        for (header, value) in endpoint.headers {
            request.setValue(value, forHTTPHeaderField: header)
        }
        var httpBody: Any? = endpoint.body
        
        if ((endpoint.method == .post || endpoint.method == .patch) && endpoint.bodyArray.count > 0) {
            httpBody = endpoint.bodyArray
        }
        
        if let httpBody = httpBody {
            do {
                let bodyData = try JSONSerialization.data(withJSONObject: httpBody, options: [])
                request.httpBody = bodyData
            }
            catch {
                debugPrint(error)
            }
        }
        
        return request
    }
}
