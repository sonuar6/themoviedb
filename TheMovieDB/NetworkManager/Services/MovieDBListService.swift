//
//  MovieDBListService.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
class MovieDBListService {
    
    typealias MovieListFetchCompletion = (_ movieListResponse: VideoDB.VideoDBResponseModel?) -> Void
    typealias MovieListFetchErrorCompletion = (_ errorResponse: ErrorModel?) -> Void
    typealias MovieListFetchErrorHandlerCompletion = (_ error: String?) -> Void



    init() {
    }
    
    public func getMovies(mov_params: VideoDBRequestModel, completion: @escaping MovieListFetchCompletion, failure: @escaping MovieListFetchErrorCompletion, errorHandler: @escaping MovieListFetchErrorHandlerCompletion){
        
        MovieDBAPIService.getMovieList(movie_param: mov_params) { (success) in
            if success == 401{
                errorHandler("Unable to fetch data")
            }
        } completionHandler: { (response) in
            completion(response)
        } failureHandler: { (errModel) in
            failure(errModel)
        } errorHandler: { (errorString) in
            errorHandler(errorString)

        }

        
        
    }

    
    
}
