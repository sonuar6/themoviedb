//
//  MovieDBEndPoints.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Alamofire


enum MovieDBEndPoints {
    // MARK: User actions
    case getVideoFiles(params: VideoDBRequestModel)


}

extension MovieDBEndPoints: Endpoint {
   
    
    var bodyArray: [Any] {
        let body_param = [[String:Any]]()
        return body_param
    }
    
  
    
    var baseURL: String {
        switch NetworkManager.networkEnviroment {
        case .dev: return Constants.URL.baseURL
        case .production: return  Constants.URL.baseURL
        case .stage: return Constants.URL.baseURL
        }
    }
    
    var path: String {
        var str_path = ""
        switch self {
        case .getVideoFiles:
            str_path = "/movie/now_playing"
            break
        }
        return str_path
    }
    
    var method: HTTPMethod {
        switch self {
        case .getVideoFiles:
            return .get
        }
    }
    
    var headers: HTTPHeaders {
        return [:]
    }
    
    var url: URL {
        switch self {
        case .getVideoFiles:
            return URL(string: self.baseURL + self.version + self.path)!
        }
    }
    
    var encoding: ParameterEncoding {
        
        switch self {
        case .getVideoFiles:
            return URLEncoding.default
        }
    }
    
    var version: String {
        return "3"
    }
    
    
    var queryParams: [Parameters] {
        var param = [Parameters]()

        switch self {
        case .getVideoFiles(let params):
            param = [["api_key": Constants.oAuth.apiKey,"language": params.language ?? "en-US","page":params.page ?? 1,"region": params.region ?? "IN"]]
        }
        
        
        return param
    }
      
    var body: Parameters {
        let param = Parameters()
        return param
    }
      
   
    
    
}
