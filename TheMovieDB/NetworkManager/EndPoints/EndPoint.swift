//
//  EndPoint.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import Alamofire

public protocol Endpoint {
    var baseURL: String { get } // https://example.com
    var path: String { get } // /users/
    var version: String { get } // /users/

    //var fullURL: String { get } // This will automatically be set. https://example.com/users/
    var method: HTTPMethod { get } // .get
    var encoding: ParameterEncoding { get } // URLEncoding.default
    var queryParams: [Parameters] { get } // Used as query parameters ["foo" : "bar"]
    var body: Parameters { get } // Used as body parameters ["foo" : "bar"]
    var bodyArray: [Any] { get } // Used as body array parameters [["foo": "bar"]]
    var headers: HTTPHeaders { get } // ["Authorization" : "Bearer SOME_TOKEN"]
    // Multipart
    var multipartBody: [MultipartParameter]? { get } // Used as body parameters for multipart request
    var multipartFiles: [MultipartFile]? { get } // Multipart files
}

public extension Endpoint {
    // The encoding's are set up so that all GET requests parameters
    // will default to be url encoded and everything else to be json encoded
    var encoding: ParameterEncoding {
        return method == .get ? URLEncoding.default : JSONEncoding.default
    }
    
    // Should always be the same no matter what
//    var fullURL: String {
//        return baseURL + path
//    }
    
    var queryParams: [Parameters] {
        return []
    }
    
    // A lot of requests don't require parameters
    // so we just set them to be empty
    var body: Parameters {
        return Parameters()
    }
    
    // A lot of requests don't require body as Array
    // so we just set them to be empty
    var bodyArray: [Any] {
        return [Any]()
    }
    
    // Multipart body parameters
    var multipartBody: [MultipartParameter]? {
        return nil
    }
    
    // Mulripart files
    var multipartFiles: [MultipartFile]? {
        return nil
    }
}


public struct MultipartParameter {
    public let key: String
    public let value: Any
    public let mimeType: String
    
    public init(key: String, value: Any, mimeType: String) {
        self.key = key
        self.value = value
        self.mimeType = mimeType
    }
}

public struct MultipartFile {
    public var data: Data
    public var name: String
    public var fileName: String
    public var mimeType: String
    
    public init(data: Data, name: String, fileName: String, mimeType: String) {
        self.data = data
        self.name = name
        self.fileName = fileName
        self.mimeType = mimeType
    }
}
