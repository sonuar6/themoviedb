//
//  VideoDBRequestModel.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import  ObjectMapper
class VideoDB{

class VideoDBResponseModel: Mappable{
    public var dates: DateResponseModel?
    public var page: Int?
    public var results: [ResultsResponseModel]?


    required public init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        dates <- map["dates"]
        page <- map["page"]
        results <- map["results"]
    }
    
    static func toObject(json: String) -> VideoDBResponseModel? {
             return Mapper<VideoDBResponseModel>().map(JSONString: json)
    }
         
    static func toObject(value: Any?) -> VideoDBResponseModel? {
             return Mapper<VideoDBResponseModel>().map(JSONObject: value)
    }
         
    static func toObjectArray(value: Any?) -> [VideoDBResponseModel]? {
             return Mapper<VideoDBResponseModel>().mapArray(JSONObject: value)
    }
 
}


class DateResponseModel: Mappable{
    public var maximum: String?
    public var minimum: String?

    required public init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        maximum <- map["maximum"]
        minimum <- map["minimum"]
    }
    
    static func toObject(json: String) -> DateResponseModel? {
             return Mapper<DateResponseModel>().map(JSONString: json)
    }
         
    static func toObject(value: Any?) -> DateResponseModel? {
             return Mapper<DateResponseModel>().map(JSONObject: value)
    }
         
    static func toObjectArray(value: Any?) -> [DateResponseModel]? {
             return Mapper<DateResponseModel>().mapArray(JSONObject: value)
    }
 
}
class ResultsResponseModel: Mappable{
    public var adult: Bool?
    public var backdrop_path: String?
    public var genre_ids: [String]?
    public var id: Int?
    public var original_language: String?
    public var original_title: String?
    public var overview: String?
    public var popularity: Double?
    public var poster_path: String?
    public var release_date: String?
    public var title: String?
    public var video: Bool?
    public var vote_average: Double?
    public var vote_count: Int?


    required public init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        adult <- map["adult"]
        backdrop_path <- map["backdrop_path"]
        genre_ids <- map["genre_ids"]
        id <- map["id"]
        original_language <- map["original_language"]
        original_title <- map["original_title"]
        overview <- map["overview"]
        popularity <- map["popularity"]
        poster_path <- map["poster_path"]
        release_date <- map["release_date"]
        title <- map["title"]
        video <- map["video"]
        vote_average <- map["vote_average"]
        vote_count <- map["vote_count"]


    }
    
    static func toObject(json: String) -> ResultsResponseModel? {
             return Mapper<ResultsResponseModel>().map(JSONString: json)
    }
         
    static func toObject(value: Any?) -> ResultsResponseModel? {
             return Mapper<ResultsResponseModel>().map(JSONObject: value)
    }
         
    static func toObjectArray(value: Any?) -> [ResultsResponseModel]? {
             return Mapper<ResultsResponseModel>().mapArray(JSONObject: value)
    }
 
    
}
}
