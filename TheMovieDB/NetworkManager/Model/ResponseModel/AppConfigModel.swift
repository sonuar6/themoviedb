//
//  AppConfigModel.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import  ObjectMapper
public class AppConfigModel: Mappable {
    public var countryList: [CountryItem]?
    public var supportedLanguages: [SupportedLanguage]?

    required public init?(map: Map) {
        
        countryList <- map["countryList"]
        supportedLanguages <- map["supportedLanguages"]
    }
    public func mapping(map: Map) {
    }
 
}
public class SupportedLanguage: Mappable {
    public var id: String?
    public var displayName: String?
    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        displayName <- map["displayName"]
    }
}

public class CountryItem: Mappable {
    //{"name":"Afghanistan","alpha-2":"AF","country-code":"004"}
    public var name: String?
    public var ISOCode: String?

    
    public required init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        ISOCode <- map["alpha-2"]
    }
}
