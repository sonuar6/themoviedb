//
//  VideoDBRequestModel.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import ObjectMapper
class VideoDBRequestModel: Mappable{
    public var page: Int?
    public var language: String?
    public var region: String?
    public var region_name: String?
    public var language_name: String?


  
    
    init(){
        
    }
    required public init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        page <- map["page"]
        language <- map["language"]
        language_name <- map["language_name"]
        region <- map["region"]
        region_name <- map["region_name"]

    }
    static func toObject(json: String) -> VideoDBRequestModel? {
             return Mapper<VideoDBRequestModel>().map(JSONString: json)
    }
         
    static func toObject(value: Any?) -> VideoDBRequestModel? {
             return Mapper<VideoDBRequestModel>().map(JSONObject: value)
    }
         
    static func toObjectArray(value: Any?) -> [VideoDBRequestModel]? {
             return Mapper<VideoDBRequestModel>().mapArray(JSONObject: value)
    }
 
}
