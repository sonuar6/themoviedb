//
//  MovieDBAPIService.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import Alamofire
struct MovieDBAPIService {
    private static var getMovieRequest: DataRequest?
    
    static func getMovieList(movie_param: VideoDBRequestModel, success: @escaping (Int) -> Void,completionHandler: @escaping (VideoDB.VideoDBResponseModel) -> Void,failureHandler: @escaping (ErrorModel) -> Void, errorHandler: @escaping (String) -> Void)
    {
        getMovieRequest?.cancel()
        getMovieRequest = NetworkManager.sharedManager().requestRestData(endpoint: MovieDBEndPoints.getVideoFiles(params: movie_param), success: success, completionHandler: completionHandler, failureHandler: failureHandler, errorHandler: errorHandler)
        
    }
    
}

