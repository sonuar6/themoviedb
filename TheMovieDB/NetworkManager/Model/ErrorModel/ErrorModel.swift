//
//  ErrorModel.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import ObjectMapper


class ErrorModel: Mappable{
    
    public var status_message: String?
    public var status_code: Int?

    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        status_message <- map["status_message"]
        status_code <- map["status_code"]
    }
    
    static func toObject(json: String) -> ErrorModel? {
             return Mapper<ErrorModel>().map(JSONString: json)
    }
         
    static func toObject(value: Any?) -> ErrorModel? {
             return Mapper<ErrorModel>().map(JSONObject: value)
    }
         
    static func toObjectArray(value: Any?) -> [ErrorModel]? {
             return Mapper<ErrorModel>().mapArray(JSONObject: value)
    }
    
    
    
}
