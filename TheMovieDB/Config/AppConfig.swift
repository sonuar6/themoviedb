//
//  AppConfig.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import ObjectMapper

public struct ConfigLocalFiles {
    public let bundleId: String
    public let appConfig: String
    
    public init(bundleId: String, appConfig: String) {
        self.bundleId = bundleId
        self.appConfig = appConfig
       
    }

}

public class MDBConfigurationManager {
    public static let sharedInstance = MDBConfigurationManager()
    private var configFiles: ConfigLocalFiles!
    private var baseURL: String!

    private init() { }
    
    public class func configure(baseURL: String, configFiles: ConfigLocalFiles) {
        sharedInstance.baseURL = baseURL
        sharedInstance.configFiles = configFiles
    }
    private func validateConfig() {
        if baseURL == nil || baseURL.isEmpty {
            preconditionFailure("Provide baseURL and it should not be Empty")
        }
        if configFiles == nil {
            preconditionFailure("ConfigFiles is not provided to fetch from Local files")
        }
    }
    
    public func fetchAppConfiguration<M: Mappable>(toClass: M.Type, forLanguage languageId: String = "en", completion: @escaping (M?) -> Void) {
        validateConfig()
        // Fetch local configuration
        let appConfigService = MDBConfigurationService<AppConfigurationService<M>>()

        let localObj = appConfigService.fetchLocalConfiguration(from: self.configFiles.appConfig, bundleId:self.configFiles.bundleId)
        //self.appConfiguration = localObj
        completion(localObj)
    }

    
    
    


}
