//
//  MDBConfigService.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//
import Foundation
import ObjectMapper


class AppConfigurationService<M: Mappable>: MDBConfigurable {
    typealias ConfigModel = M
    
   
    
    static func getLocalConfigFileName() -> String {
        return "app_configuration_v1"
    }
    
    static func parseConfig(data: Data) -> M? {
        if let dataStr = String(data: data, encoding: .utf8) {
            let cleanStr = MTFileUtils.removeComments(from: dataStr)
            let appConfigModel = M(JSONString: cleanStr)
            return appConfigModel
        }
        
        /*do {
            if let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let appConfigModel = AppConfigModel(JSON: "jsonData")
                return appConfigModel
            }
        } catch {
            print(error)
        }*/
        return nil
    }
    
}

open class MTFileUtils {
    public class func removeComments(from str: String) -> String {
        // (/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)
        // (\\/\\*[\\s\\S]*?\\*\\/)|(([^:]|^)//.*$)
        // (\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+)|(([^:]|^)//.*) // Working but replacing extra '//' behind
        let regex = try! NSRegularExpression(pattern: "(\\/\\*[\\s\\S]*?\\*\\/)|(([^:]|^)//.*)", options: NSRegularExpression.Options.caseInsensitive)

        let range = NSRange(location: 0, length: str.count)
        let modString = regex.stringByReplacingMatches(in: str, options: [], range: range, withTemplate: "")
        return modString
    }
}










protocol MDBConfigurable {
    
    /// Assign this with Type needed in adopted class
    associatedtype ConfigModel
    
    /// Provides configuration cache file name to use to read/write
    ///
    /// - Returns: String - Name to use for configuration cache file
   
    /// Provides configuration file name which is local in Bundle
    ///
    /// - Returns: String - Local configuration file name
    static func getLocalConfigFileName() -> String
        
    /// Parses Data to given ConfigModel Type
    ///
    /// - Parameter data: Data to parse
    /// - Returns: ConfigModel Type parsed from Data
    static func parseConfig(data: Data) -> ConfigModel?
    
   
}

class MDBConfigurationService<T: MDBConfigurable> {
    
    private var configModel: T.ConfigModel?
    init() { }
    
   
    func fetchLocalConfiguration(from fileName: String = T.getLocalConfigFileName(), bundleId: String = "com.mycase.MDBConfiguration") -> T.ConfigModel? {
        guard let bundle = Bundle(identifier: bundleId), let path = bundle.path(forResource: fileName, ofType: "json") else {
            return nil
        }
        
        let fileURL = URL(fileURLWithPath: path)
        do {
            let configData = try Data(contentsOf: fileURL)
            configModel = T.parseConfig(data: configData)
            return configModel
        }
        catch {
            print(error)
        }
        return nil
    }

}
