//
//  Notification+Name.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation

public extension Notification.Name {
    static let didChangeLanguage     = Notification.Name("DidChangeLanguage")
    static let didChangeTheme        = Notification.Name("DidChangeTheme")
}
