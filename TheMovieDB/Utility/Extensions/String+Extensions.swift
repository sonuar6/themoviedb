//
//  String+Extensions.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
extension String{
    func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
}
