//
//  UIImageView+Extensions.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import UIKit
import  AlamofireImage
extension UIImageView{
    
    func placeAtTheCenterWithView(view: UIView) {
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0))
    }
    
    func setImageWithURL(url : String? , placeholder : String?)  {
        guard let urlString = url, let urlQueryString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            self.image = UIImage(named: placeholder!)
            return
        }
        //        let activityIndicator = UIActivityIndicatorView()
        //        activityIndicator.style = .gray
        // activityIndicator.frame = self.bounds
        if self.tag == 11{
            // activityIndicator.center = CGPoint(x:UIScreen.main.bounds.size.width/2, y: 90.0)
        }
        if let url = URL(string: urlQueryString) {
            //            self.addSubview(activityIndicator)
            //            self.placeAtTheCenterWithView(view: activityIndicator)
            //            activityIndicator.startAnimating()
            self.af_setImage(withURL: url,placeholderImage:UIImage(named: placeholder!), filter: nil, imageTransition: UIImageView.ImageTransition.crossDissolve(0.5), completion: { (image) in
                //                activityIndicator.stopAnimating()
                //                activityIndicator.isHidden = true
                //                activityIndicator.removeFromSuperview()
                
                if let newImage = image.value {
                    self.image = newImage
                    
                }
                else{
                    self.image = UIImage(named: placeholder!)
                }
            })
        }
    }
}
