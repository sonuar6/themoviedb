//
//  UIViewController+FromStoryboard.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
import UIKit

public protocol StoryboardLoadable where Self: UIViewController {
    static var storyboardName: String { get }
    static var storyboardId: String { get }
    static func instantiateFromStoryboard() -> Self
}

public extension StoryboardLoadable {
    static var storyboardId: String {
        return "\(self)"
    }
    
    static func instantiateFromStoryboard() -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: storyboardId) as! Self
    }
    
    
        func showToast(message : String) {

               //self.view.frame.size.height-100
               let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height - 100, width: self.view.frame.size.width - 40, height: 35))
               toastLabel.backgroundColor = UIColor.white.withAlphaComponent(0.5)
               toastLabel.textColor = UIColor.black
               toastLabel.font = UIFont(name: "Euphoria-Regular", size: 10.0)
               toastLabel.textAlignment = .center;
               toastLabel.text = message
               toastLabel.alpha = 1.0
               toastLabel.layer.cornerRadius = 10;
               toastLabel.clipsToBounds  =  true
               self.view.addSubview(toastLabel)
               UIView.animate(withDuration: 4.0, delay: 0.02, options: .curveEaseOut, animations: {
                    toastLabel.alpha = 0.0
               }, completion: {(isCompleted) in
                   toastLabel.removeFromSuperview()
               })
            
          
            
    }
    
    
    
}
