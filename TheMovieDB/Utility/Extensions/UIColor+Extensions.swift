//
//  UIColor+Extensions.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import  UIKit
public extension UIColor {
    
    convenience init(hex: String) {
        let hexString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let rValue = Int(color >> 16) & mask
        let gValue = Int(color >> 8) & mask
        let bValue = Int(color) & mask
        
        let red   = CGFloat(rValue) / 255.0
        let green = CGFloat(gValue) / 255.0
        let blue  = CGFloat(bValue) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
    
    func toHexString() -> String {
        var rValue: CGFloat = 0
        var gValue: CGFloat = 0
        var bValue: CGFloat = 0
        var alphaComponent: CGFloat = 0
        
        getRed(&rValue, green: &gValue, blue: &bValue, alpha: &alphaComponent)
        
        let rgb: Int = (Int)(rValue*255)<<16 | (Int)(gValue*255)<<8 | (Int)(bValue*255)<<0
        
        return String(format: "#%06x", rgb)
    }
}
