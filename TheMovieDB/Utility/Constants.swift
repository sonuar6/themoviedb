//
//  Constants.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation

struct Constants {
    
}


extension Constants {
    struct URL {
        #if TheMovieDBDev
        static let baseURL = "https://api.themoviedb.org/"
        static let baseImageURL = "http://image.tmdb.org/t/p/w185"
        #else
        static let baseURL = "https://api.themoviedb.org/"
        static let baseImageURL = "http://image.tmdb.org/t/p/w185"
        #endif
    }
    
    struct oAuth {
        #if TheMovieDBDev
        static let apiKey = "34c902e6393dc8d970be5340928602a7"
        #else
        static let apiKey = "34c902e6393dc8d970be5340928602a7"
        #endif
    }
    
    struct UserData {
        static let filter = "FIlter"
    }

    
    
}
