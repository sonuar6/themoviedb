//
//  MovieManager.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
 class Moviemanager {
    static let shared: Moviemanager = Moviemanager()
    let movieService: MovieDBListService = MovieDBListService()
    var videoFilter: VideoDBRequestModel?
}



extension Moviemanager {
    func getMovieListBaseFilter() -> VideoDBRequestModel? {
        if let base = UserDefaults.standard.object(forKey: Constants.UserData.filter) as? String {
            let baseModel = VideoDBRequestModel(JSONString: base)
            return baseModel
        }
        return nil
    }
    func setMovieListBaseFilter(_ base: VideoDBRequestModel?) {
        if let base_data = base {
            UserDefaults.standard.set(base_data.toJSONString(), forKey: Constants.UserData.filter)
        }
        else {
            UserDefaults.standard.removeObject(forKey: Constants.UserData.filter)
        }
    }
    
    
    
    
    
}



extension Moviemanager {
    
    func getMovies(movie_param: VideoDBRequestModel, completion: ((VideoDB.VideoDBResponseModel?) -> Void)?, failure: ((String?) -> Void)?){
        
        movieService.getMovies(mov_params: movie_param) { (response) in
            completion!(response)
        } failure: { (errmodel) in
            failure!(errmodel?.status_message)
        } errorHandler: { (errorString) in
            failure!(errorString)
        }
    }
    
}


