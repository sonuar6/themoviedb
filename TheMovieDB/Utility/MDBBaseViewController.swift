//
//  MDBBaseViewController.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import UIKit
public protocol Localizable {
    //override this function and reload all static and dynamic localizable content in the UI
    func localizeUI()
}

public protocol Themeable {
    //override this function and reload all colors and fonts & bg images and theme related content in the UI
    func applyTheme()
}



open class MDBBaseViewController: UIViewController, StoryboardLoadable {
    
    open var isNavBarHidden: Bool = false

    

    public static var storyboardName: String {
        return getStroyboardName()
    }

    open class func getStroyboardName() -> String {
        preconditionFailure("All subclasses must override localizeUI()")
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotification()
        localizeUI()
        applyTheme()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Localization
    @objc open func localizeUI() {
        preconditionFailure("All subclasses must override localizeUI()")
    }
    
    // MARK: - Apply Theme
    @objc open func applyTheme() {
        preconditionFailure("All subclasses must override applyTheme()")
    }
    
   
    open func registerForNotification() {
        // Register for Language change notification
        NotificationCenter.default.addObserver(self, selector: #selector(MDBBaseViewController.localizeUI), name: .didChangeLanguage, object: nil)
        // Register for Theme change notification
        NotificationCenter.default.addObserver(self, selector: #selector(MDBBaseViewController.applyTheme), name: .didChangeTheme, object: nil)
    }

}
