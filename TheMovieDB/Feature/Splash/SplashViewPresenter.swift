//
//  SplashViewPresenter.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import Foundation
import UIKit

protocol SplashView: class {
    func showBaseFilterList()
    func showSelectionError(_ err: String)
    func showVideoView(_ viewController: UIViewController)
}

public class SplashViewPresenter {
    weak private var splashView: SplashView?
    var config: AppConfigModel?
    var region: CountryItem?
    var language: SupportedLanguage?
    
    init() {}
   
    func attachSplashView(_ view: SplashView) {
        self.splashView = view
    }
    
    func detachView() {
        self.splashView = nil
    }
    
    func validate()-> Bool{
        var isValid = true
        var str = ""
        if let country = self.region?.ISOCode, country.isEmpty{
             isValid = false
            str = "Please select the region"
        }
        if let lang = language?.id, lang.isEmpty{
             isValid = false
            str = "Please select the language"
        }
        if !isValid{
        self.splashView?.showSelectionError(str)
        }
        return isValid
    }
    
    
    
    func setAppFilter(){
        if validate(){
            let filter = VideoDBRequestModel()
            filter.language = self.language?.id
            filter.region = self.region?.ISOCode
            filter.region_name = self.region?.name
            filter.language_name = self.language?.displayName

            filter.page = 1
            Moviemanager.shared.videoFilter = filter
            Moviemanager.shared.setMovieListBaseFilter(filter)
            transition()
        }
    }
    
    func startApp(){
        if let baseData = Moviemanager.shared.getMovieListBaseFilter(){
            Moviemanager.shared.videoFilter = baseData
            transition()

        }else{
            MDBConfigurationManager.sharedInstance.fetchAppConfiguration(toClass: AppConfigModel.self) { [weak self](config) in
                guard let strongSelf = self else{return}
                strongSelf.config = config
                strongSelf.splashView?.showBaseFilterList()
            }
        }
   
    }
    
    func transition(){
        let targetVc = MDBMoviesListVC.instantiateFromStoryboard()
        let navController = UINavigationController(rootViewController: targetVc)
        navController.isNavigationBarHidden = true
        self.splashView?.showVideoView(navController)
    }



    
}
