//
//  MDBSplashVC.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import UIKit

class MDBSplashVC: MDBBaseViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var logoCenterConst: NSLayoutConstraint!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var logoWidthConst: NSLayoutConstraint!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var logoHgtConst: NSLayoutConstraint!
    
    @IBOutlet weak var submitButton: UIButton!
    private let splashViewPresenter: SplashViewPresenter = SplashViewPresenter()
    private var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    override class func getStroyboardName() -> String {
        return "Splash"
    }
    
    override func localizeUI() {
        self.captionLabel.text = "Select the region"
        submitButton.setTitle("Get Started", for: .normal)
    }
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    override func applyTheme() {
        logoImageView.image = UIImage(named: "moviedb")
        self.captionLabel.textColor = .white
        self.captionLabel.textColor = UIColor(hex: "#2bbad4")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splashViewPresenter.attachSplashView(self)
        setUpUI()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.splashViewPresenter.startApp()
        }
        
    }
    
    func setUpUI(){
        self.captionLabel.isHidden = true
        self.listTableView.isHidden = true
        self.submitButton.isHidden = true

        self.submitButton.alpha =  0
        self.captionLabel.alpha =  0
        self.listTableView.alpha =  0
    }
    
    
    func showFilterView(show: Bool){
        self.logoCenterConst.constant = -220
        self.logoHgtConst.constant =  100
        self.logoWidthConst.constant =  100
    
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0.0, options: [.allowAnimatedContent], animations:  { [self] in
            self.view.layoutIfNeeded()
            self.captionLabel.isHidden =  false
            self.listTableView.isHidden =  false

            UIView.animate(withDuration: 0.6, delay: 0,
                           options: [.curveEaseInOut],
                           animations: {
                            self.captionLabel.alpha =  1
                            self.listTableView.alpha =  1

                            self.listTableView.reloadData()

            }, completion:nil)
        })

    }
    @IBAction func submitPressed(_ sender: Any) {
        self.splashViewPresenter.setAppFilter()
    }
    
}

extension MDBSplashVC: SplashView{
    func showBaseFilterList() {
        showFilterView(show: true)
    }
    func showSelectionError(_ err: String) {
        self.showToast(message: err)
    }
    func showVideoView(_ viewController: UIViewController) {
        appDelegate.window?.rootViewController = viewController
    }
}

extension MDBSplashVC: UITableViewDelegate, UITableViewDataSource
{
   
    func numberOfSections(in tableView: UITableView) -> Int {
      return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if self.splashViewPresenter.region == nil{
            if let itemCount = self.splashViewPresenter.config?.countryList?.count, itemCount > 0{
                count = itemCount
            }
        }else{
            if let itemCount = self.splashViewPresenter.config?.supportedLanguages?.count, itemCount > 0{
                count = itemCount
            }
        }
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let optionCell = tableView.dequeueReusableCell(withIdentifier: "CountryTableCell") as! CountryTableCell
        if self.splashViewPresenter.region == nil{
        if let countryItems = self.splashViewPresenter.config?.countryList{
            let country = countryItems[indexPath.row]
            var country_name = ""
            if let country_flag = country.name?.flag(country: country.ISOCode ?? ""){
                country_name = country_flag
            }
            if let name = country.name{
                country_name = "\(country_name)  \(name)"
            }
            optionCell.titleLabel.text = country_name
        }
        }else{
            if let languages = self.splashViewPresenter.config?.supportedLanguages{
                let lang = languages[indexPath.row]
                if let name = lang.displayName{
                    optionCell.titleLabel.text = name
                }
            }
        }
        return optionCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.splashViewPresenter.region == nil{
            if let countryItems = self.splashViewPresenter.config?.countryList{
                let country = countryItems[indexPath.row]
                self.splashViewPresenter.region = country
                self.captionLabel.text = "Select Language"
                self.listTableView.reloadData()
            }
        }else{
            if let languages = self.splashViewPresenter.config?.supportedLanguages{
                let lang = languages[indexPath.row]
                self.submitButton.isHidden =  false
                self.submitButton.alpha =  1
                self.splashViewPresenter.language = lang
            }
        }
        
    }

}
