//
//  MovieTableCell.swift
//  TheMovieDB
//
//  Created by emcuser on 11/07/2021.
//

import UIKit
import FloatRatingView

class MovieTableCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    


    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        // Initialization code
    }
    
    func setMovieCell(movies :VideoDB.ResultsResponseModel){
        self.titleLabel.text = movies.original_title
        self.dateLabel.text = movies.release_date
        self.infoLabel.text = movies.overview
        self.ratingView.rating = (movies.vote_average ?? 0.0) / 2
        self.movieImageView.setImageWithURL(url: "\(Constants.URL.baseImageURL)\(movies.poster_path ?? "")", placeholder: "moviedb")
    }
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
