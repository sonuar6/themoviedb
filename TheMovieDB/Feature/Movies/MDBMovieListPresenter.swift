//
//  MDBMovieListPresenter.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import Foundation
protocol MDBMovieListPresenterDelegate: class {
    func showMovieList(movies: [VideoDB.ResultsResponseModel])
    func showError(err:String)
}

class MDBMovieListPresenter {
    
    private weak var movieListView: MDBMovieListPresenterDelegate?
    
    private var movieList: VideoDB.VideoDBResponseModel?{
        didSet {
            guard let movies = movieList?.results, movies.count > 0  else {
                self.movieListView?.showError(err: "No result found")
                return
            }
            self.movieListView?.showMovieList(movies: movies)
        }
    }
    
    var currentPage = 1
       

    init() {
    }
    func attachMovieListView(_ view: MDBMovieListPresenterDelegate) {
        self.movieListView = view
    }
    
    func detachView() {
        self.movieListView = nil
    }
    
    
    func getMovies() {
        
        let requestModel = VideoDBRequestModel(JSON: [:])
        requestModel?.language = Moviemanager.shared.videoFilter?.language
        requestModel?.region = Moviemanager.shared.videoFilter?.region
        requestModel?.page = self.currentPage
        if let model = requestModel{
            Moviemanager.shared.getMovies(movie_param: model) { (response) in
                self.movieList = response
            } failure: {[weak self] (errors) in
                guard let strongSelf = self else {return}
                strongSelf.movieListView?.showError(err: "No result found")
            }
        }
        
        
    }
    
}

