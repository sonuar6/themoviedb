//
//  MDBMoviesListVC.swift
//  TheMovieDB
//
//  Created by emcuser on 10/07/2021.
//

import UIKit

class MDBMoviesListVC: MDBBaseViewController {
    
    @IBOutlet weak var bgVIew: UIView!
    @IBOutlet weak var movieTable: UITableView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    
    
    private var movies = [VideoDB.ResultsResponseModel]()
    private var isRefreshingList:Bool = false
    
    override class func getStroyboardName() -> String {
        return "Movies"
    }
    
    override func applyTheme() {
        self.view.backgroundColor = UIColor(hex: "#212c39")
        self.bgVIew.backgroundColor = UIColor(hex: "#F8F8F8")

    }
    
    override func localizeUI() {
        self.countryLabel.text = Moviemanager.shared.videoFilter?.region_name
        self.languageLabel.text = Moviemanager.shared.videoFilter?.language_name?.uppercased()
    }
        
    private var viewPresenter = MDBMovieListPresenter()


    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        viewPresenter.attachMovieListView(self)
        getMovieData()
    }
    
    private func setUpUI(){
        bgVIew.layer.cornerRadius = 30
        bgVIew.clipsToBounds = true
        bgVIew.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    @IBAction func refreshPressed(_ sender: Any) {
        Moviemanager.shared.setMovieListBaseFilter(nil)
        let filterVC = MDBSplashVC.instantiateFromStoryboard()
        filterVC.modalPresentationStyle = .fullScreen
        self.present(filterVC, animated: true, completion: nil)
        
    }
    
    func getMovieData(){
        viewPresenter.getMovies()
    }
    
}

extension MDBMoviesListVC: UITableViewDelegate, UITableViewDataSource
{
   
    func numberOfSections(in tableView: UITableView) -> Int {
      return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if  self.movies.count > 0{
            count = self.movies.count
        }
        
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movCell = tableView.dequeueReusableCell(withIdentifier: "MovieTableCell") as! MovieTableCell
            let mov = self.movies[indexPath.row]
            movCell.setMovieCell(movies: mov)
        return movCell
        
    }
    
   
}
extension MDBMoviesListVC: UIScrollViewDelegate{
    func loadMoreItemsForList(){
        self.viewPresenter.currentPage += 1
        getMovieData()
        
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isRefreshingList){
                self.isRefreshingList = true
                self.loadMoreItemsForList()
            }
        }
    
    
    
}



extension MDBMoviesListVC :MDBMovieListPresenterDelegate{
    func showError(err: String) {
        self.showToast(message: err)
    }
    func showMovieList(movies: [VideoDB.ResultsResponseModel]) {
        self.isRefreshingList = false
        self.movies.append(contentsOf: movies)
        self.movieTable.reloadData()
    }
}
