# TheMovieDB
Designed to list out movies information listed by TheMovieDB  from their API 



# Getting Started

Clone the file from git clone git clone https://sonuar6@bitbucket.org/sonuar6/themoviedb.git to your local machine and open with XCode
to see the skeleton and to execute the project.


# Built With

Swift 5.4 ,
Xcode 12.4.



# API Support

TheMovieDB .GET API responses with the following filters.

Query Params

Param "page" : Specify which page to query.
Param "language" :Pass a ISO 639-1 value to display translated data for the fields that support it. 
Param "region" : Specify a ISO 3166-1 code to filter release dates. Must be uppercase.
Param "api_key": API KEY obtained







# Support Libraries and Dependency

Multiple targets for Development and Production
Cocoapods shared dependency,
Alamofire,
Activity,
XCTest and Sanitizer.


Demo with use case are as of shown below.

![]()










